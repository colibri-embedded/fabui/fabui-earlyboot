import "@babel/polyfill";
import Vue from "vue";

import "./plugins/vuetify";

import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./plugins/websocket";
import "./plugins/axios";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store,
  data: {}
}).$mount("#app");
