import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "material-design-icons/iconfont/material-icons.css";
import "../assets/css/roboto-font.css";

Vue.use(Vuetify, {});
